// FROM HTML file - <input type="checkbox" id="dark-mode" /> 

var ButtonWillBeChanged = document.getElementById("darkMode"); //gets the mode from the HTML file


if (sessionStorage.getItem("mode") == "dark") {  //check storage if dark mode was on or off
  darkmode(); //if dark mode was on, run this function
} else {
  nodark(); //else run this function
}


ButtonWillBeChanged.addEventListener("change", function () { //if the checkbox state is changed, run a function
 
  if (ButtonWillBeChanged.checked) {   //check if the checkbox is checked or not
    darkmode(); //if the checkbox is checked, run this function
  } else {
    nodark(); //else run this function
  }});


function darkmode() { //function for checkbox when checkbox is checked
  document.body.classList.add("darkMode"); //add a class to the body tag
  ButtonWillBeChanged.checked = true; //set checkbox to be checked state
  sessionStorage.setItem("mode", "dark"); //store a name & value to know that dark mode is on
}


function nodark() { //function for checkbox when checkbox is not checked
  document.body.classList.remove("darkMode"); //remove added class from body tag
  ButtonWillBeChanged.checked = false; //set checkbox to be unchecked state
  sessionStorage.setItem("mode", "light"); //store a name & value to know that dark mode is off or light mode is on
}
